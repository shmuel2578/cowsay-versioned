#!/bin/bash

docker build -t cowsay-versioned:1.0 .
docker run -d -p 8081:8080 --name cowsay_testing cowsay-versioned:1.0
